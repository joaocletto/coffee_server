# Projeto Padrão para django na SSYS

A idéia principal desse projeto é ter uma organização para 
arquivos de um projeto Django. Sempre que um projeto Django 
iniciar, pode-se começar com um fork deste projeto.

Ainda não há apps no projeto, e o projeto é chamado core_proj.

## Roadmap 

- [ ] Criação de script que mude os "nomes" do projeto

  > Alterar tudo no settings, pastas e outros, substituindo core_proj pelo nome desejado.

- [ ] Definição posicionamento de APPs Django
  > Tendo a entender que colocar os apps dentro de uma subpasta comum dentro de django > core_projeto > apps seja melhor, mas isso é meu gosto pessoal. Precisamos definir juntos.

- [ ] Removido toda e qualquer referência ao Celery, precisamos deixar?
-  [ ] Tendo a entender que precisamos usar o Gunicorn em staging ou mesmo em dev,
  > Tivemos surpresas no wise por conta de os devs estarem usando uma coisa diferente do que está no deploy, isso não é muito legal. Temos que ter ambientes iguais. 

- [ ] Estabelecer versões nos requirements.txt
