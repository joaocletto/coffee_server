from core_proj.models import BaseModel

from django.db import models


class Cart(BaseModel):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    products = models.ManyToManyField('products.Product')
