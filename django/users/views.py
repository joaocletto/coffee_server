from rest_framework import status, exceptions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.tokens import RefreshToken

from django.utils.translation import gettext_lazy as _

from users.serializers import UserLoginSerializer, UserOutputSerializer
# Create your views here.
class UserLoginView(APIView):

    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']

        refresh = RefreshToken.for_user(user)
        refresh_token = str(refresh)
        access_token = str(refresh.access_token)

        return Response({
            'user': UserOutputSerializer(user).data,
            'refresh': refresh_token,
            'access_token': access_token,
        }, status=status.HTTP_200_OK)
