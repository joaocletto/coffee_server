"""User managers"""
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    """User manager for managing users."""
    use_in_migrations = True

    def _create_user(self, email: str, password: str, **extra_fields):
        if not email:
            raise ValueError(_('Endereço de e-mail é obrigatório para um usuário.'))

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, email: str, password: str = None, **extra_fields):
        """
        Creates a new user with the specified email and password.

        Parameters:
            email (str): The email address of the user.
            password (str, optional): The password for the user. If not provided,
            a random password will be generated.
            **extra_fields (dict): Additional fields for the user.

        Returns:
            User: The newly created user object.
        """
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email: str, password: str, **extra_fields):
        """
        Creates a superuser with the given email and password.

        Parameters:
            email (str): The email address of the superuser.
            password (str): The password for the superuser.
            **extra_fields: Additional fields for the superuser.

        Raises:
            ValueError: If the `is_staff` or `is_superuser` fields are not set to True.

        Returns:
            User: The created superuser.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
