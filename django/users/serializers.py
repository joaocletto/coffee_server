from rest_framework import serializers

from django.db import transaction
from django.utils.translation import gettext_lazy as _
from users.models import User


class UserInputSerializer(serializers.ModelSerializer):
    """User input serializer."""
    email = serializers.EmailField(required=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
            'email',
            'password',
        )

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

class UserOutputSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'email',
        )


class UserLoginSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """User login serializer."""
    email = serializers.EmailField(required=False)
    password = serializers.CharField(write_only=True)

    def validate(self, attrs: dict) -> dict:
        """
        Validates the given data for login.

        Parameters:
            data (dict): A dictionary containing the login data.
                - email (str): The email of the user.
                - password (str): The password of the user.

        Raises:
            serializers.ValidationError: If the email or password is missing or invalid.

        Returns:
            dict: The validated login data.
        """
        email = attrs.get('email', None)
        password = attrs.get('password', None)

        if not email or not password:
            raise serializers.ValidationError(_('É necessário informar email e senha para realizar o login.'))

        email = attrs.get('email', None)

        user = User.objects.filter(email=email, is_active=True).first()

        if user is None:
            error_message =  _('Email não encontrado.')
            raise serializers.ValidationError(error_message)

        if not user.check_password(password):
            raise serializers.ValidationError('Senha incorreta')

        attrs['user'] = user

        return attrs
