
from core_proj.models import BaseModel

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import UserManager


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    """
    User model
    """
    USERNAME_FIELD = 'email'

    email = models.EmailField(_('Endereço de e-mail'), max_length=255, unique=True)
    is_staff = models.BooleanField(
        _('Staff Status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('Ativo'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.',
        ),
    )

    objects = UserManager()

    class Meta:
        """
        User model meta
        """
        verbose_name = _('Usuário')
        verbose_name_plural = _('Usuários')
