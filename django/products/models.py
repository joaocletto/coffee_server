from core_proj.models import BaseModel

from django.db import models


class Product(BaseModel):
    """Product model."""

    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='products')

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
