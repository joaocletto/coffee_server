from carts.viewsets import CartViewSet
from orders.viewsets import OrderViewSet
from products.viewsets import ProductViewSet
from users.viewsets import UserViewSet
from rest_framework.routers import DefaultRouter, SimpleRouter

from django.conf import settings

router = DefaultRouter() if settings.DEBUG else SimpleRouter()

router.register(r'carts', CartViewSet, basename='carts-api')
router.register(r'orders', OrderViewSet, basename='orders-api')
router.register(r'products', ProductViewSet, basename='products-api')
router.register(r'users', UserViewSet, basename='users-api')

api_urls = router.urls
