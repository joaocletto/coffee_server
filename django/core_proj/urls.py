"""core_proj URL Configuration."""
from rest_framework.permissions import AllowAny

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from .routers import api_urls
from users.views import UserLoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
    path('api/login/', UserLoginView.as_view(permission_classes=[AllowAny]), name='api-login'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
    # Getting media files for debug environment.
    urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + urlpatterns
