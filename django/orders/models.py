from core_proj.models import BaseModel

from django.db import models


# Create your models here.
class Order(BaseModel):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField()
    products = models.ManyToManyField('products.Product')

    def __str__(self):
        return self.name
